<?php
/**
 * User: alaa
 * Date: 8/7/2018
 * Time: 5:06 PM
 */

namespace Modules\Pages\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;


class PagesRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'TCG\Voyager\Models\Page';
    }
}
