const { mix } = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/themes/mytheme/assets/js/app.js', 'js/pages.js')
    .sass( __dirname + '/Resources/themes/mytheme/assets/sass/app.scss', 'css/pages.css');

if (mix.inProduction()) {
    mix.version();
}
