@extends('theme::layouts.master')

@section('page_title')
    | {{ $page->title }}
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/pages.css') }}">
@stop

@section('javascript')
    <script src="{{ asset('js/pages.js') }}"></script>
@stop
@section('content')
    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
        <header class="masthead mb-auto">
            <div class="inner">
                <h3 class="masthead-brand">{{setting('site.title')}}</h3>
                <nav class="nav nav-masthead justify-content-center">
                    <a class="nav-link active" href="#">Home</a>
                    <a class="nav-link" href="#">Features</a>
                    <a class="nav-link" href="#">Contact</a>
                </nav>
            </div>
        </header>

        <main role="main" class="inner cover">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1 class="cover-heading">{{ $page->title }}</h1>
                        <div class="lead">
                            <div class="row">
                                <div class="col-12">
                                    <img src="{{ Storage::disk(config('voyager.storage.disk'))->url($page->image) }}" class="img-circle" width="300" alt="">
                                </div>
                                <div class="col-12">
                                    {!! $page->body !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <footer class="mastfoot mt-auto">
            <div class="inner">
                <p>{!! setting('site.footer_message') !!}</p>
            </div>
        </footer>
    </div>
@stop
