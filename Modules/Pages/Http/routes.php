<?php

Route::group(
    [
        'middleware' => ['web'],
        'prefix' => 'pages',
        'namespace' => 'Modules\Pages\Http\Controllers',
    ], function () {

        // Index page
        Route::get('/', 'PagesController@index');

        //View page by its slug
        Route::get('/{slug}', 'PagesController@show');
});

