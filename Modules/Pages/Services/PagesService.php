<?php
/**
 * Created by PhpStorm.
 * User: alaa
 * Date: 8/7/2018
 * Time: 5:09 PM
 */
namespace Modules\Pages\Services;

use Modules\Pages\Repositories\PagesRepository;

class PagesService {

    private $page;

    public function __construct(PagesRepository $pagesRepository)
    {
        $this->page = $pagesRepository;
    }

    public function findPageBySlug($slug) {
        $page = $this->page->findBy('slug', $slug);
        if($page) {
            return $page;
        }
        return null;
    }
}
