<?php

namespace Modules\Book\Providers;

use Illuminate\Support\ServiceProvider;
use ReCaptcha\ReCaptcha;
use Illuminate\Support\Facades\Validator;


class RecaptchaValidatorServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        Validator::extend('captcha', function ($attribute, $value, $parameters, $validator) {
            $ip = request()->getClientIp();
            $recaptcha = new ReCaptcha(env('INVISIBLE_RECAPTCHA_SECRETKEY'));
            $resp = $recaptcha->verify($value, $ip);

            return $resp->isSuccess();
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
