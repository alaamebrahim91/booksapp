<?php
/**
 * User: alaa
 * Date: 8/7/2018
 * Time: 5:06 PM
 */

namespace Modules\Book\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;


class BookRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Modules\Book\Entities\Book';
    }
}
