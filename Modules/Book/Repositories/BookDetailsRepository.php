<?php
/**
 * User: alaa
 * Date: 8/7/2018
 * Time: 5:06 PM
 */

namespace Modules\Book\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Support\Facades\DB;


class BookDetailsRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Modules\Book\Entities\BookDetails';
    }

    public function getAllTranslationsBySlug($slug) {
        return DB::table('book_details')
            ->leftJoin('languages', 'book_details.language_id', '=', 'languages.id')
            ->rightJoin('books','book_details.title_id','=', 'books.id')
            ->select('book_details.*', 'languages.native', 'languages.name')
            ->where('books.slug', '=',$slug)
            ->get();

    }

    public function count(){
        return DB::table('book_details')->count('*');
    }
}
