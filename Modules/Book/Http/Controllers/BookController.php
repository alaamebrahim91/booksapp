<?php

namespace Modules\Book\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Modules\Admin\Services\SitemapService;
use Modules\Book\Services\BookService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;
use Nwidart\Modules\Json;

class BookController extends Controller
{

    private $bookService;
    private $sitemapService;
    private $slug = 'brief-islam-book';

    public function __construct(BookService $bookService, SitemapService $sitemapService)
    {
        $this->bookService = $bookService;
        $this->sitemapService = $sitemapService;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($slug)
    {
        $book = $this->bookService->findBookBySlug($slug);
        if ($book != null) {
            $bookDetails = $this->bookService->findAllTranslations($slug);
            if ($bookDetails != null) {
                return view('theme::show')->withBook($book)->withBookDetails($bookDetails);
            }
            // If no details added yet
            return view('theme::no-data', compact('book'));
        } else {
//            $bookDetails = $this->bookService->findBookTranslationBySlug($slug);
//            if($bookDetails != null) {
//                return view('theme::show-translation', compact( 'bookDetails'));
//            }
            // If no details added yet
            abort(404);
        }

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function showWithoutSlug()
    {
        $this->sitemapService->createSitemap();
        $book = $this->bookService->findBookBySlug($this->slug);
        if ($book != null) {
            $bookDetails = $this->bookService->findAllTranslations($this->slug);
            if ($bookDetails != null) {
                return view('theme::show')->withBook($book)->withBookDetails($bookDetails);
            }
            // If no details added yet
            return view('theme::no-data', compact('book'));
        } else {
//            $bookDetails = $this->bookService->findBookTranslationBySlug($slug);
//            if($bookDetails != null) {
//                return view('theme::show-translation', compact( 'bookDetails'));
//            }
            // If no details added yet
            abort(404);
        }

    }

    public function getBooks($slug) {
        $bookDetails = $this->bookService->findAllTranslations($slug);
        return \response()->json($bookDetails);
    }

    public function downloadHighQualityFile($id) {
        if(is_numeric($id) == true) {
            $bookDetails = $this->bookService->findBookDetailsById($id);
            if($bookDetails != null) {
                //$file =  Storage::disk(config('voyager.storage.disk'))->url($bookDetails->h_quality_file);
                $bookdownload = json_decode($bookDetails->h_quality_file);
                $file = storage_path(DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR .$bookdownload[0]->download_link); // or wherever you have stored your PDF files
                return response()->download($file);
            } else {
                return null;
            }
        }else {
            return null;
        }

    }

    public function downloadLowQualityFile($id) {
        if(is_numeric($id) == true) {
            $bookDetails = $this->bookService->findBookDetailsById($id);
            if($bookDetails != null) {
//            return Storage::disk(config('voyager.storage.disk'))->download($bookDetails->l_quality_file);
                $bookdownload = json_decode($bookDetails->l_quality_file);
                $file = storage_path(DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR .$bookdownload[0]->download_link); // or wherever you have stored your PDF files
                return response()->download($file);
            } else {
                return null;
            }
        }else {
            return null;
        }
    }

    public function updateDownloadsCount($id) {
        $updated =  $this->bookService->updateDownloadsCount($id);
        if ($updated) {
            return new JsonResponse([
                'success' => true
            ]);
        }
        return new JsonResponse([
            'success' => false
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('book::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
