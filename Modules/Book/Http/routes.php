<?php

Route::group([
    'middleware' => 'web',
    'prefix' => LaravelLocalization::setLocale() . 'book',
    'namespace' => 'Modules\Book\Http\Controllers'
    ], function()
{
    Route::get('/', 'BookController@index');

    //View page by its slug
    Route::get('/{slug}', 'BookController@show')->name('books.show');

    Route::get('/download-hq/{id}', 'BookController@downloadHighQualityFile');
    Route::get('/download-lq/{id}', 'BookController@downloadLowQualityFile');

    //Api Requests
    Route::group([
        'middleware' =>  'cors',
        'prefix' => '/api'
    ], function () {
        Route::get('/get-translations/{slug}', 'BookController@getBooks');
        Route::get('/update-downloads/{id}', 'BookController@updateDownloadsCount');

    });

});


