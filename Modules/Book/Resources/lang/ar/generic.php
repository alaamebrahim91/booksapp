<?php

return [
    "no-data" => "لم يتم إضافة بيانات الكتاب حتى الآن",
    "contact-us" => "تواصل معنا",
    "view" => "عرض",
    "readmore" => "المزيد...",
    "description" => "نبذة عن الكتاب",
    "language" => "لغة الكتاب",
    "downloads-count" => "عدد مرات التحميل",
    "hqd" => "جودة عالية",
    "lqd" => "جودة الطباعة",
    "count-books" => "عدد الكتب",
    "choose-language" => "اختر اللغة",
    "count-translations" => "عدد الترجمات",
    "count-translations-text" => "يوجد لديك عدد :count ترجمة/ترجمات بقاعدة البيانات الخاصة بالتطبيق. انقر على الزر أدناه للعرض",
    "dimmer-button" => "عرض جميع الترجمات"

];
