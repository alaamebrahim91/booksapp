@extends('theme::layouts.master')

@section('page_title')
    | {{ $page->title }}
@stop

@section('css')

@stop

@section('javascript')

@stop
@section('content')
    <section class="probootstrap-intro probootstrap-intro-inner" style="background-image: url({{asset('img/hero_bg_1_b.jpg')}});" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-12 probootstrap-intro-text">
                    <h1 class="probootstrap-animate" data-animate-effect="fadeIn">{{$page->title}}</h1>
                    <div class="probootstrap-subtitle probootstrap-animate" data-animate-effect="fadeIn">

                    </div>
                </div>

            </div>
        </div>
        <span class="probootstrap-animate"><a class="probootstrap-scroll-down js-next" href="#next-section">للأسفل <i class="icon-chevron-down"></i></a></span>
    </section>
    <main>
        <section id="next-section" class="probootstrap-section">
            {{ trans('book::generic.no-data') }}
        </section>

    </main>
@stop
