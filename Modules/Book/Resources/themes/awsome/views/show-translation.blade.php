@extends('theme::layouts.master')

@section('page_title')
    | {{ $bookDetails->title }}
@stop
@section('meta')
    <meta name="description" content="@if($bookDetails->meta_description) {{ $bookDetails->meta_description }} @else setting('site.description') @endif"/>
    <meta name="keywords" content="@if($bookDetails->meta_keywords) {{ $bookDetails->meta_keywords }} @else setting('site.keywords') @endif"/>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('css/book.css')}}">
@stop

@section('javascript')
    <script src="{{asset('js/book.min.js')}}"></script>
@stop
@section('content')
    <main>
        <section id="next-section" class="probootstrap-section">
            <div class="container">
                <div class="row probootstrap-gutter60 mb50">
                    <div class="col-md-4">
                        <figure>
                            <img src="{{ Storage::disk(config('voyager.storage.disk'))->url($bookDetails->cover_image) }}" lt="{{ $bookDetails->title }}" class="img-responsive">
                        </figure>
                        <div class="row">
                            <div class="col-12" id="app">
                                <download-book
                                    v-bind:hq-file="{{ json_encode($bookDetails->h_quality_file)  }}"
                                    v-bind:lq-file="{{ json_encode($bookDetails->l_quality_file)  }}"
                                ></download-book>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3 class="probootstrap-heading text-info">{{ $bookDetails->title }}</h3>
                        <hr>

                        <div class="row">
                            <div class="col-12">
                                <h5 class="text-info">{{ trans('book::generic.description') }}</h5>
                                {!! $bookDetails->description !!}
                                <hr>
                            </div>
                            <div class="col-12 book-description">
                                <h5 class="text-info">{{ trans('book::generic.language') }}</h5>
                                    {{ $bookDetails->language->name }}
                                <hr>
                            </div>
                            <div class="col-12">
                                <h5 class="text-info">{{ trans('book::generic.downloads-count') }}</h5>
                                {{ $bookDetails->language->downloads }}
                                <hr>
                            </div>
                            <div class="col-12"></div>
                        </div>

                    </div>
                    <div class="col-md-3"></div>
                </div>

            </div>
        </section>
        <section class="probootstrap-section" dir="rtl">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 probootstrap-relative">
                        <h2 class="probootstrap-heading mt0 mb50">My Programs</h2>
                        <ul class="probootstrap-owl-navigation absolute right">
                            <li><a href="#" class="probootstrap-owl-prev"><i class="icon-chevron-left"></i></a></li>
                            <li><a href="#" class="probootstrap-owl-next"><i class="icon-chevron-right"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 probootstrap-relative">
                        <div class="owl-carousel owl-carousel-carousel">
                            <div class="item">
                                <div class="probootstrap-program">
                                    <a href="#"><img src="{{ Storage::disk(config('voyager.storage.disk'))->url($bookDetails->cover_image) }}" alt="Free Bootstrap Template by uicookies.com" class="img-responsive img-rounded"></a>
                                    <h3>Dumbbell Squat</h3>
                                    <p>Sets: 3, Reps: 8-10, Rest: 30 sec.</p>
                                    <p>She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="probootstrap-program">
                                    <a href="#"><img src="{{ Storage::disk(config('voyager.storage.disk'))->url($bookDetails->cover_image) }}" alt="Free Bootstrap Template by uicookies.com" class="img-responsive img-rounded"></a>
                                    <h3>Push Up</h3>
                                    <p>Sets: 3, Reps: 8-10, Rest: 30 sec.</p>
                                    <p>She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="probootstrap-program">
                                    <a href="#"><img src="{{ Storage::disk(config('voyager.storage.disk'))->url($bookDetails->cover_image) }}" alt="Free Bootstrap Template by uicookies.com" class="img-responsive img-rounded"></a>
                                    <h3>Reverse Lunge</h3>
                                    <p>Sets: 3, Reps: 8-10, Rest: 30 sec.</p>
                                    <p>She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="probootstrap-program">
                                    <a href="#"><img src="{{ Storage::disk(config('voyager.storage.disk'))->url($bookDetails->cover_image) }}" alt="Free Bootstrap Template by uicookies.com" class="img-responsive img-rounded"></a>
                                    <h3>Dumbbell Overhead Press</h3>
                                    <p>Sets: 3, Reps: 8-10, Rest: 30 sec.</p>
                                    <p>She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="probootstrap-program">
                                    <a href="#"><img src="{{ Storage::disk(config('voyager.storage.disk'))->url($bookDetails->cover_image) }}" alt="Free Bootstrap Template by uicookies.com" class="img-responsive img-rounded"></a>
                                    <h3>Bulgarian Split Squat</h3>
                                    <p>Sets: 3, Reps: 8-10, Rest: 30 sec.</p>
                                    <p>She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="probootstrap-program">
                                    <a href="#"><img src="{{ Storage::disk(config('voyager.storage.disk'))->url($bookDetails->cover_image) }}" alt="Free Bootstrap Template by uicookies.com" class="img-responsive img-rounded"></a>
                                    <h3>DIP</h3>
                                    <p>Sets: 3, Reps: 8-10, Rest: 30 sec.</p>
                                    <p>She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="probootstrap-program">
                                    <a href="#"><img src="{{ Storage::disk(config('voyager.storage.disk'))->url($bookDetails->cover_image) }}" alt="Free Bootstrap Template by uicookies.com" class="img-responsive img-rounded"></a>
                                    <h3>Russian Twist</h3>
                                    <p>Sets: 3, Reps: 8-10, Rest: 30 sec.</p>
                                    <p>She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
@stop
