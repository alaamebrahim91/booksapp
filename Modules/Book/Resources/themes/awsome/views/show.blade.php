@extends('theme::layouts.master')
@inject('bookService', 'Modules\Admin\Services\LanguagesService')

@section('page_title')| {{ $bookService->translate($book->title , 'database','book.title') }} @stop

@section('meta')
    <meta name="description" content="@if($book->meta_description) {{ $bookService->translate($book->meta_description, 'database', 'book.meta-description') }} @else {{ $bookService->translate('site.description', 'settings') }} @endif"/>
    <meta name="keywords" content="@if($book->meta_keywords) {{ $bookService->translate($book->meta_keywords, 'database', 'book.meta-keywords') }} @else {{ $bookService->translate('site.keywords', 'settings') }}  @endif"/>
@stop

@section('css')
@stop

@section('javascript')
    <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit" async
            defer></script>
    <script src="{{ asset('js/book.min.js') }}"></script>
@stop
@section('content')
    <header class="masthead">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-lg-7 my-auto">
                    <div class="header-content mx-auto">
                        <h1 class="mb-5">{{ $bookService->translate($book->title, 'database', 'book.title') }}</h1>
                        <a href="#about-book" class="btn btn-outline js-scroll-trigger">{{ $bookService->translate('generic.about-book', 'translation') }}</a>
                    </div>
                </div>
                <div class="col-lg-5 my-auto">
                    <div class="device-container">
                        <div class="device-mockup iphone6_plus portrait white">
                            <div class="device">
                                <div class="screen">
                                    <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                                    <img src="{{ Storage::disk(config('voyager.storage.disk'))->url($book->cover_image) }}" class="img-fluid" alt="">
                                </div>
                                <div class="button">
                                    <a href="#about-book" class="btn btn-outline js-scroll-trigger"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="download bg-primary text-center" id="about-book">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <div class="section-heading">
                        <h2>{{ $bookService->translate('generic.about-book', 'translation') }}</h2>
                        <div class="text-justify">{{ $bookService->translate($book->description, 'database', 'book.description') }}</div>
                        <div class="text-left text-white">{{ $bookService->translate($book->written_by, 'database', 'book.written_by') }}</div>
                    </div>
                    <div class="badges">
                        <a href="#download-book" class="btn btn-outline btn-xl js-scroll-trigger">{{ $bookService->translate('generic.dbook', 'translation') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="features" id="download-book">
        <div class="container" id="app">
            <div class="text-center">
                <h2>{{ $bookService->translate('generic.dbook', 'translation') }}</h2>
                <hr>
            </div>
            <download-book
                :slug="'{{ json_encode($book->slug)  }}'"
                :url="'{{ env('APP_URL') . '/book/api/get-translations/' .  $book->slug}}'"
                :appurl="'{{ env('APP_URL') }}'"
                :chooselanguage="'{{ $bookService->translate("generic.choose-language", "translation") }}'"
                :highquality="'{{ $bookService->translate("book::generic.hqd", "translation") }}'"
                :lowquality="'{{ $bookService->translate("book::generic.lqd", "translation") }}'"
            ></download-book>
        </div>
    </section>

    {{--<section class="cta">--}}
        {{--<div class="cta-content">--}}
            {{--<div class="container">--}}
                {{--<h2>Stop waiting.<br>Start building.</h2>--}}
                {{--<a href="#contact" class="btn btn-outline btn-xl js-scroll-trigger">Let's Get Started!</a>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="overlay"></div>--}}
    {{--</section>--}}
    {{--<section class="contact bg-primary" id="contact">--}}
        {{--<div class="container">--}}
            {{--<h2>We--}}
                {{--<i class="fa fa-heart"></i>--}}
                {{--new friends!</h2>--}}
            {{--<ul class="list-inline list-social">--}}
                {{--<li class="list-inline-item social-twitter">--}}
                    {{--<a href="#">--}}
                        {{--<i class="fa fa-twitter"></i>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="list-inline-item social-facebook">--}}
                    {{--<a href="#">--}}
                        {{--<i class="fa fa-facebook"></i>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="list-inline-item social-google-plus">--}}
                    {{--<a href="#">--}}
                        {{--<i class="fa fa-google-plus"></i>--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    {{--</section>--}}
@stop
