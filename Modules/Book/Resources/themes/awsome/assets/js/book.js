window.Vue = require('vue');
import VueRecaptcha from 'vue-recaptcha';
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('download-book', require('./components/DownloadBook.vue'));
Vue.component('vue-recaptcha', VueRecaptcha);

const app = new Vue({
    el: '#app'
});
