<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoogleTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('locale_id')->unsigned();
            $table->foreign('locale_id')->references('id')->on('languages');
            $table->text('code');
            $table->text('type');
            $table->longText('translation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_translations');
    }
}
