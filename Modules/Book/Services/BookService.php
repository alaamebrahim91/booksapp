<?php
/**
 * Created by PhpStorm.
 * User: alaa
 * Date: 8/7/2018
 * Time: 5:09 PM
 */

namespace Modules\Book\Services;

use Modules\Book\Repositories\BookRepository as Book;
use Modules\Book\Repositories\BookDetailsRepository as BookDetails;
use Modules\Admin\Repositories\LanguageRepository as Language;
use Modules\Admin\Repositories\GoogleTranslationsRepository as Google;
use Stichoza\GoogleTranslate\TranslateClient;
use Mcamara\LaravelLocalization\LaravelLocalization;

class BookService
{

    private $book;
    private $bookDetails;
    private $language;
    private $ll;

    public function __construct(Book $book, BookDetails $bookDetails, Language $language, LaravelLocalization $ll, Google $google)
    {
        $this->book = $book;
        $this->bookDetails = $bookDetails;
        $this->language = $language;
        $this->google = $google;
        $this->ll = $ll;

    }

    public function addLanguages()
    {
        foreach (config('laravellocalization.supportedLocales') as $key => $lang) {
            $new = [
                'name' => $lang['name'],
                'script' => $lang['script'],
                'native' => $lang['native'],
                'regional' => $lang['regional'],
                'key' => $key
            ];
            $this->language->create($new);
        }
    }

    public function findBookBySlug($slug)
    {
        $book = $this->book->findBy('slug', $slug);
        if ($book) {
            return $book;
        }
        return null;
    }

    public function findAllTranslations($slug)
    {
        return $this->bookDetails->getAllTranslationsBySlug($slug);
    }

    public function findBookTranslationBySlug($slug)
    {
        $book = $this->bookDetails->findBy('slug', $slug);
        if ($book) {
            return $book;
        }
        return null;
    }

    public function findBookDetailsByBookId($bookId)
    {
        $bookDetails = $this->bookDetails->findBy('title_id', $bookId);
        if ($bookDetails) {
            return $bookDetails;
        }
        return null;
    }

    public function findBookDetailsById($id)
    {
        $bookDetails = $this->bookDetails->findBy('id', $id);
        if ($bookDetails) {
            return $bookDetails;
        }
        return null;
    }

    public function count()
    {
        return $this->bookDetails->count();
    }

    public function updateDownloadsCount($id)
    {
        if (!session()->exists('bdc' . $id)) { //Books downloads count
            $prevDownloads = $this->bookDetails->findBy('id', $id);
            $prevDownloads = ($prevDownloads == null) ? 0 : $prevDownloads;
            $this->bookDetails->update(['downloads' => $prevDownloads->downloads + 1], $id);
            session()->put('bdc' . $id, $id);
            return true;
        }
        return true;
    }
}
