<?php

namespace Modules\Book\Entities;

use Illuminate\Database\Eloquent\Model;

class BookDetails extends Model
{
    protected $fillable = [];

    public function language(){
        return $this->belongsTo('Modules\Book\Entities\Language');
    }

}
