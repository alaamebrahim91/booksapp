<?php

namespace Modules\Book\Entities;

use Illuminate\Database\Eloquent\Model;
//use TCG\Voyager\Traits\Translatable;

class Book extends Model
{
//    use Translatable;
//    protected $translatable  = ['title', 'description', 'h_quality_file', 'l_quality_file', 'cover_photo'];
    protected $table = 'books';
    protected $fillable = [];
}
