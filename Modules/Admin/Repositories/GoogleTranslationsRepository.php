<?php
/**
 * User: alaa
 * Date: 8/7/2018
 * Time: 5:06 PM
 */

namespace Modules\Admin\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Support\Facades\DB;


class GoogleTranslationsRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Modules\Admin\Entities\Googletranslations';
    }

    public function findByCodeAndLocaleId($code, $locale_id, $columns = array('*'))
    {
        $data = DB::table('google_translations')
            ->where('code', '=', $code)
            ->where('locale_id', '=', $locale_id)
            ->get();
        return $data;
    }

    public function deleteByCode($code)
    {
        DB::table('google_translations')->where('code', '=', $code)->delete();
    }

    public function deleteByType($type)
    {
        DB::table('google_translations')->where('type', '=', $type)->delete();
    }
}
