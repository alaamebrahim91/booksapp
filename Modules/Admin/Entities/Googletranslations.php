<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class Googletranslations extends Model
{
    protected $table= 'google_translations';
    protected $fillable = ['locale_id', 'code', 'type', 'translation'];
}
