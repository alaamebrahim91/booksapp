<?php

namespace Modules\Admin\Events;

use Illuminate\Queue\SerializesModels;
use TCG\Voyager\Models\Setting;

class SettingUpdated
{
    use SerializesModels;

    public $setting;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Setting $setting)
    {
        $this->setting = $setting;
        //
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
