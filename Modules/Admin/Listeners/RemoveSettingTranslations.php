<?php

namespace Modules\Admin\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Admin\Events\SettingUpdated;
use Modules\Admin\Services\LanguagesService;

class RemoveSettingTranslations
{
    private $languageService;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(LanguagesService $languageService)
    {
        $this->languageService = $languageService;
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SettingUpdated $event)
    {
        $this->languageService->deleteTranslationByType('settings');
        //
    }
}
