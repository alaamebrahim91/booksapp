var gulp = require('gulp');
var sass = require('gulp-sass');
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var pkg = require('./package.json');
var browserSync = require('browser-sync').create();
var mix = require('laravel-mix');

// Set the banner content
var banner = ['/*!\n',
    ' * Start Bootstrap - <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n',
    ' * Copyright 2013-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
    ' * Licensed under <%= pkg.license %> (https://github.com/BlackrockDigital/<%= pkg.name %>/blob/master/LICENSE)\n',
    ' */\n',
    ''
].join('');

// Copy third party libraries from /node_modules into /vendor
gulp.task('vendor', function() {

    // Bootstrap
    gulp.src([
        './node_modules/bootstrap/dist/**/*',
        '!./node_modules/bootstrap/dist/css/bootstrap-grid*',
        '!./node_modules/bootstrap/dist/css/bootstrap-reboot*'
    ])
        .pipe(gulp.dest('./public/vendor/bootstrap'))

    // Font Awesome
    gulp.src([
        './node_modules/font-awesome/**/*',
        '!./node_modules/font-awesome/{less,less/*}',
        '!./node_modules/font-awesome/{scss,scss/*}',
        '!./node_modules/font-awesome/.*',
        '!./node_modules/font-awesome/*.{txt,json,md}'
    ])
        .pipe(gulp.dest('./public/vendor/font-awesome'))

    // jQuery
    gulp.src([
        './node_modules/jquery/dist/*',
        '!./node_modules/jquery/dist/core.js'
    ])
        .pipe(gulp.dest('./public/vendor/jquery'))

    // jQuery Easing
    gulp.src([
        './node_modules/jquery.easing/*.js'
    ])
        .pipe(gulp.dest('./public/vendor/jquery-easing'))

    // Simple Line Icons
    gulp.src([
        './node_modules/simple-line-icons/fonts/**',
    ])
        .pipe(gulp.dest('./public/vendor/simple-line-icons/fonts'))

    gulp.src([
        './node_modules/simple-line-icons/css/**',
    ])
        .pipe(gulp.dest('./public/vendor/simple-line-icons/css'))

});

// Compile SCSS
gulp.task('css:compile', function() {
    return gulp.src('./resources/views/themes/frontend/awsome/assets/scss/*.scss')
        .pipe(sass.sync({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(gulp.dest('./public/css'))
});

gulp.task('css:voyager', function() {
    return gulp.src('./vendor/tcg/voyager/resources/assets/sass/*.scss')
        .pipe(sass.sync({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(gulp.dest('./public/vendor/tcg/voyager/assets/css'))
});

// Minify CSS
gulp.task('css:minify', ['css:compile'], function() {
    return gulp.src([
        './public/css/*.css',
        '!./public/css/*.min.css'
    ])
        .pipe(cleanCSS())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./public/css'))
        .pipe(browserSync.stream());
});

//copy mockups
gulp.task('copy-mockups', function() {
    gulp.src([
        './resources/views/themes/frontend/awsome/assets/device-mockups/*.*',
        './resources/views/themes/frontend/awsome/assets/device-mockups/**/*.*',
        './resources/views/themes/frontend/awsome/assets/device-mockups/**/**/*.*',
    ])
        .pipe(gulp.dest('./public/device-mockups'));

    gulp.src([
        './resources/views/themes/frontend/awsome/assets/img/*.*',
    ])
        .pipe(gulp.dest('./public/img'));
});

//copy plain css
gulp.task('copy-css', function () {
    gulp.src('./resources/views/themes/frontend/awsome/assets/css/**')
        .pipe(gulp.dest('./public/css'));
})

// CSS
gulp.task('css', ['css:compile', 'css:minify', 'copy-css', 'css:voyager']);

// Minify JavaScript
gulp.task('js:minify', function() {
    return gulp.src([
        './public/js/*.js',
        '!./public/js/*.min.js'
    ])
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./public/js'))
        .pipe(browserSync.stream());
});

// JS
gulp.task('js', ['js:minify']);

// Default task
gulp.task('default', ['css', 'js', 'vendor']);

// Configure the browserSync task
gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});


// Dev task
gulp.task('dev', ['css', 'js', 'vendor', 'copy-mockups'], function() {
    gulp.watch('./resources/views/themes/frontend/awsome/assets/scss/*.scss', ['css']);
    gulp.watch('./resources/views/themes/frontend/awsome/assets/js/*.js', ['js']);
    // gulp.watch('./*.html', browserSync.reload);
});
