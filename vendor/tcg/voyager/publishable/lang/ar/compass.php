<?php

return [
    'welcome'                => 'لا تقم بتغيير أى شيئ هنا إن لم تكن المبرمج أو وصلت هنا عن طريق الصدفة',
    'links'         => [
        'title'                 => 'الروابط',
        'documentation'         => 'Documentation',
        'voyager_homepage'      => 'Voyager Homepage',
        'voyager_hooks'         => 'Voyager Hooks',
    ],
    'commands'      => [
        'title'                 => 'الأوامر',
        'text'                  => 'Run Artisan Commands from Voyager.',
        'clear_output'          => 'clear output',
        'command_output'        => 'Artisan Command Output',
        'additional_args'       => 'Additional Args?',
        'run_command'           => 'Run Command',
    ],
    'resources'     => [
        'title'                 => 'المصادر',
        'text'                  => 'تساعد المصادر على الوصول لما تريده بسرعة.',

    ],
    'logs'          => [
        'title'                 => 'السجلات',
        'text'                  => 'السجلات الخاصة بالبرنامج',
        'file_too_big'          => 'حجم الملف اصبح أكبر من 50 ميجابايت من فضلك قم بتحميله',
        'level'                 => 'مستوى',
        'context'               => 'Context',
        'date'                  => 'تاريخ',
        'content'               => 'المحتوى',
        'download_file'         => 'تحميل الملف',
        'delete_file'           => 'حذف الملف',
        'delete_all_files'      => 'حذف جميع الملفات',
        'delete_success'        => 'تم حذف الملف بنجاح:',
        'delete_all_success'    => 'م حذف جميع الملفات بنجاح!',

    ],
    'fonts'         => [
        'title'                 => 'الخطوط',
        'font_class'            => 'Voyager Fonts Class Mapping',
        'font_character'        => 'Voyager Fonts Character Mapping',
    ],
];
