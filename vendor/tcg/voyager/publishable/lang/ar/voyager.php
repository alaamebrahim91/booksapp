<?php

return [
    'date' => [

    ],

    'generic' => [
        "locale" => "اللغة"
    ],

    'login' => [

    ],

    'profile' => [

    ],

    'settings' => [

    ],

    'media' => [

    ],

    'menu_builder' => [

    ],

    'post' => [

    ],

    'bread' => [

    ],

    'database' => [

    ],

    'dimmer' => [

    ],

    'form' => [

    ],

    'datatable' => [

    ],

    'theme' => [

    ],

    'json' => [

    ],

    'analytics' => [

    ],

    'error' => [

    ],
];
