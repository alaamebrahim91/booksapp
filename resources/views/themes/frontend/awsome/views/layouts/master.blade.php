<!DOCTYPE html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @yield('meta')
    <title> {{ setting('site.title') }}  @yield('page_title')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.0.0/css/bootstrap.min.css" integrity="sha384-P4uhUIGk/q1gaD/NdgkBIl3a6QywJjlsFJFk7SPRdruoGddvRVSwv5qFnvZ73cpz" crossorigin="anonymous">
    <script src="https://cdn.rtlcss.com/bootstrap/v4.0.0/js/bootstrap.min.js" integrity="sha384-54+cucJ4QbVb99v8dcttx/0JRx4FHMmhOWi4W+xrXpKcsKQodCBwAvu3xxkZAwsH" crossorigin="anonymous"></script> -->

    {{--<!-- Custom fonts for this template -->--}}
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    {{--<!-- Plugin CSS -->--}}
    <link rel="stylesheet" href="{{ asset('device-mockups/device-mockups.min.css') }}">

    {{--<!-- Custom styles for this template -->--}}
    @if(in_array(LaravelLocalization::getCurrentLocale(), ['ar', 'ur']) )
        <link href="{{ asset('css/new-age.min.css') }}" rel="stylesheet">
    @else
        <link href="{{ asset('css/new-age-ltr.min.css') }}" rel="stylesheet">
        <style>
            .dropdown-menu {
                height: 200px;
                overflow-y: auto;
                text-align: center;
            }
        </style>
    @endif
    @yield('css')
</head>

@if(in_array(LaravelLocalization::getCurrentLocale(), ['ar', 'ur', 'fa']) )
<body id="page-top" dir="rtl">
@else
<body id="page-top" dir="ltr">
@endif


@include('themes.frontend.awsome.views.partials.navbar')

@yield('content')

@include('themes.frontend.awsome.views.partials.footer')


{{--<!-- Bootstrap core JavaScript -->--}}
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
{{--<!-- Plugin JavaScript -->--}}
<script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
{{--<!-- Custom scripts for this template -->--}}
<script src="{{ asset('js/new-age.min.js') }}"></script>
@yield('javascript')
{!! setting('site.google_analytics_code') !!}
</body>

</html>
