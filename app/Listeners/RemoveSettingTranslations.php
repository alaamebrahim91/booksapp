<?php

namespace App\Listeners;

use App\Events\settingUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Book\Services\BookService;

class RemoveSettingTranslations
{

    private $bookService;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }

    /**
     * Handle the event.
     *
     * @param  settingUpdated  $event
     * @return void
     */
    public function handle(settingUpdated $event)
    {
        //
    }
}
