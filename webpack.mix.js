let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');
//
// mix.options({
//     processCssUrls: false
// }).sass('vendor/tcg/voyager/resources/assets/sass/app.scss', 'vendor/tcg/voyager/publishable/assets/css')
//     .js('vendor/tcg/voyager/resources/assets/js/app.js', 'vendor/tcg/voyager/publishable/assets/js');
mix.setPublicPath('public/');
mix.options({processCssUrls: false});
// mix.setResourceRoot('../');
// mix.scripts([
//     './resources/views/themes/frontend/trainer/assets/js/vendor/jquery.min.js',
//     './resources/views/themes/frontend/trainer/assets/js/vendor/jquery.easing.1.3.js',
//     './resources/views/themes/frontend/trainer/assets/js/vendor/jquery.stellar.min.js',
//     './resources/views/themes/frontend/trainer/assets/js/vendor/jquery.flexslider-min.js',
//     './resources/views/themes/frontend/trainer/assets/js/vendor/jquery.countTo.js',
//     './resources/views/themes/frontend/trainer/assets/js/vendor/jquery.appear.min.js',
//     './resources/views/themes/frontend/trainer/assets/js/vendor/jquery.magnific-popup.min.js',
//     './resources/views/themes/frontend/trainer/assets/js/vendor/owl.carousel.min.js',
//     './resources/views/themes/frontend/trainer/assets/js/vendor/bootstrap.min.js',
//     './resources/views/themes/frontend/trainer/assets/js/vendor/jquery.waypoints.min.js'
// ], './public/js/scripts/scripts.js');
// mix.js('./resources/views/themes/frontend/trainer/assets/js/main.js', './public/js/main.min.js');
mix.js('./Modules/Book/Resources/themes/awsome/assets/js/book.js', './public/js/book.min.js');
//
// //CSS
// mix.sass('./resources/views/themes/frontend/trainer/assets/scss/style.scss', './public/css')
//     .sass('./Modules/Book/Resources/themes/trainer/assets/sass/book.scss', './public/css');
// mix.styles([
//     './resources/views/themes/frontend/trainer/assets/css/vendor/bootstrap.min.css',
//     './resources/views/themes/frontend/trainer/assets/css/vendor/animate.css',
//     './resources/views/themes/frontend/trainer/assets/css/vendor/icomoon.css',
//     './resources/views/themes/frontend/trainer/assets/css/vendor/flexslider.css',
//     './resources/views/themes/frontend/trainer/assets/css/vendor/owl.carousel.min.css',
//     './resources/views/themes/frontend/trainer/assets/css/vendor/owl.theme.default.min.css',
//     './resources/views/themes/frontend/trainer/assets/css/vendor/magnific-popup.css',
//     './resources/views/themes/frontend/trainer/assets/css/vendor/photoswipe.css',
//     './resources/views/themes/frontend/trainer/assets/css/vendor/default-skin.css',
// ], './public/css/styles-merged.css');
//
// mix.copy('./resources/views/themes/frontend/trainer/assets/img/**/*.{gif,jpg,png,svg}', 'public/img');
// mix.copy('./resources/views/themes/frontend/trainer/assets/fonts/**/*.{eot,ttf,woff,svg}', 'public/fonts');
// mix.copy('./resources/views/themes/frontend/trainer/assets/fonts/**/ico*.{eot,ttf,woff,svg}', 'public/fonts/icomoon');
// mix.copy('./node_modules/@mdi/font/fonts/*.{eot,ttf,woff,svg}', 'public/fonts');
