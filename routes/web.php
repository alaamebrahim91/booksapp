<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
////    return redirect(route('books.show', 'brief-islam-book'));
//    return redirect(route('books.show', 'brief-islam-book'));
//});

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ], function () {
    Route::get('/', '\Modules\Book\Http\Controllers\BookController@showWithoutSlug')->name('public.index');
});

Route::group(['prefix' => LaravelLocalization::setLocale() . '/admin'], function () {
    Voyager::routes();

    //Logs viewer
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('admin.logs');

    //Sitemap creator
    Route::get('sitemaps', '\Modules\Admin\Http\Controllers\SitemapController@index')->name('admin.sitemap.index');

    $namespacePrefix = '\\Modules\\Admin\\Http\\Controllers\\Overrides\\';
    //Overrides
    // Settings
    Route::group([
        'middleware' => 'admin.user',
        'as' => 'voyager.settings.',
        'prefix' => 'settings',
    ], function () use ($namespacePrefix) {

        Route::post('/', ['uses' => $namespacePrefix . 'SettingsController@store', 'as' => 'store']);
        Route::put('/', ['uses' => $namespacePrefix . 'SettingsController@update', 'as' => 'update']);
    });
});
